console.log("Repetição While");
let numero = 0;
while(numero<=15){
    if(numero%2==0){
        console.log(`Valor nr: ${(numero)} é PAR!`);
    }else{
        console.log(`Valor nr: ${(numero)} é IMPAR!`);
    }
    numero++;
}

console.log("REPETIÇÃO DO/WHILE");
let numero1 = 11;
do{
    console.log(`Valor nr: ${(numero1)}`);
    numero1++;
}while(numero1<=0);

console.log("REPETIÇÃO FOR");
for(let numero2=0; numero2<=10; numero2++){
    console.log(`valor nr: ${(numero2)}`);
}


